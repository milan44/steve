<?php
define('SOURCE_FILE', 'data.txt');

require "functions.php";

// Refresh cache when GET parameter "g" is set or the cache file does'nt exist
if (isset($_GET["g"]) || !file_exists(SOURCE_FILE))
{
    VichanDataLoader::rebuildData();
}

$generated = Steve::generateSentences();
?>
<html>
    <head>
        <title>Steve</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="./rita-full.min.js"></script>
        <script src="./compromise.min.js"></script>
        <style>
            pre {
                white-space: pre-wrap;
                word-wrap: break-word;
            }
            html:not([dir]) pre {
                unicode-bidi: plaintext;
            }
            p {
                margin: 3px 0;
            }
            p:not(:first-child) {
                border-top: 1px solid #666;
                padding-top: 3px;
            }
        </style>
        <script>
            // Passing the original text to the javascript.
            const data = `<?php echo str_replace("`", "", NormalizeHelper::prepareTextForFrontend(Steve::$text)); ?>`;
        </script>
    </head>
    <body>
        <!-- Outputting the generated text. -->
        <pre id="php"><?php echo trim($generated); ?></pre>

        <!-- Letting JavaScript also try to generate some text using markov chains. -->
        <br>
        <br>
        <br>
        <pre id="javascript">Loading...</pre>
        <script>
            function ucFirst(text) {
                return text.charAt(0).toUpperCase() + text.substring(1);
            }

            const rm = new RiMarkov(4, true, false);

            // Loading text
			rm.loadText(data);

			// Generating 4 sentences
			const sentences = rm.generateSentences(4);
			let html = '';
			
			sentences.forEach(function(sentence) {
				html += '<p>' + ucFirst(nlp(sentence).normalize().out('text')) + '</p>';
			});
			
			document.getElementById('javascript').innerHTML = html;

			const lines = document.getElementById('php').innerText.split("\n");
			html = '';
			
			lines.forEach(function(line) {
				html += '<p>' + ucFirst(nlp(line).normalize().out('text')) + '</p>';
			});
			
			document.getElementById('php').innerHTML = html;
        </script>
    </body>
</html>
