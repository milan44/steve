<?php
$classes = dirname(__FILE__) . '/classes/';

require $classes . 'Markov.php';
require $classes . 'Helper.php';
require $classes . 'NormalizeHelper.php';
require $classes . 'RegexHelper.php';
require $classes . 'VichanDataLoader.php';
require $classes . 'Steve.php';

function showText($text, $title) {
	echo '<html><head><title>'.$title.'</title><meta name="viewport" content="width=device-width, initial-scale=1">';
	
	include 'meta.html';
	
	echo '<style>
	pre {
		white-space: pre-wrap;
		word-wrap: break-word;
	}
	html:not([dir]) pre {
		unicode-bidi: plaintext;
	}
</style>';
	
	echo '</head><body><pre>'.$text.'</pre></body></html>';
}