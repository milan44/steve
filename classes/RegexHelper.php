<?php
/**
 * Class which has a bunch of functions for easier Regex handling.
 */
class RegexHelper
{
    /**
     * Removes ass Regex matches from a given string.
     * @param string $regex
     * @param string $string
     * @return string
     */
    public static function remove(string $regex, string $string) : string
    {
        return preg_replace($regex, "", $string);
    }

    /**
     * Checks if a given Regex has any match in the given string.
     * @param string $regex
     * @param string $string
     * @return bool
     */
    public static function check(string $regex, string $string) : bool
    {
        $string = trim($string);
        return preg_replace($regex, '', $string) !== $string;
    }
}