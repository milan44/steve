<?php
/**
 * Class which has a bunch of helpful functions.
 */
class Helper
{
    /**
     * Checks if $haystack ends with $needle.
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function endsWith(string $haystack, string $needle) : bool
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    /**
     * Checks if a string contains a substring
     * @param string $hay
     * @param string $needle
     * @return bool
     */
    public static function includes(string $hay, string $needle) : bool
    {
        return strpos($hay, $needle) !== false;
    }

    /**
     * Function for listing json files recursively
     * @param string $dir
     * @return array
     */
    public static function listJsonFiles(string $dir) : array
    {
        $ffs = scandir($dir);
        $list = [];
        foreach ($ffs as $ff){
            if ($ff != '.' && $ff != '..')
            {

                if (Helper::endsWith($ff, ".json"))
                {
                    $list[] = $dir.'/'.$ff;
                }
                if(is_dir($dir.'/'.$ff))
                {
                    $list = array_merge($list, Helper::listJsonFiles($dir.'/'.$ff));
                }
            }
        }
        return $list;
    }
}