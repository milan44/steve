<?php
/**
 * Class which has a bunch of functions to normlize text.
 */
class NormalizeHelper
{
    /**
     * Makes sure a string will be displayed correctly in the frontend.
     * @param string $text
     * @return string
     */
    public static function prepareTextForFrontend(string $text) : string
    {
        $text = preg_replace('/>(\s*)/m', '', $text);
        $text = preg_replace('/(?<![.!?])$/m', '.', $text);
        $text = str_replace('.', ".\n", $text);
        $text = str_replace('!', "!\n", $text);
        $text = str_replace('?', "?\n", $text);

        $text = preg_replace('/[^a-z ,.?!\'0-9\n]/mi', '', $text);

        $text = preg_replace('/^([^a-z]*)$/mi', '', $text);
        $text = preg_replace('/^([^a-z]*)$/mi', '', $text);
        $text = preg_replace('/^([^a-z]*)$/mi', '', $text);

        $text = str_replace("\n\n", "\n", $text);

        $text = preg_replace('/^( +)|( +)$/m', '', $text);
        $text = preg_replace('/( {2,}})/m', ' ', $text);

        $text = preg_replace('/(?<![a-z0-9,]) | (?![a-z0-9,])/mi', '', $text);

        $nText = '';
        $text = explode("\n", $text);

        foreach($text as $t) {
            $c = sizeof(explode(' ', $t));

            if ($c > 1) {
                if (!empty($nText)) {
                    $nText .= "\n";
                }
                $nText .= $t;
            }
        }

        $text = $nText;
        return $text;
    }

    /**
     * Removes lines that were already used
     * @param string $text
     */
    public static function removeUsedLines(string &$text)
    {
        $lines = explode("\n", $text);
        $newLines = [];
        foreach($lines as $line) {
            if(!exec('grep '.escapeshellarg(trim($line)).' ./steve/steve.txt')) {
                $newLines[] = $line;
            }
        }
        $text = implode("\n", $newLines);
    }

    /**
     * Does some initial normalizing (When reading the post).
     * @param string $text
     * @return string
     */
    public static function initialNormalize(string $text) : string
    {
        NormalizeHelper::removeLinks($text);
        NormalizeHelper::resolveHTML($text);
        NormalizeHelper::removeReferences($text);
        NormalizeHelper::removeInvalidCharacters($text);
        NormalizeHelper::lowercaseTheUpercase($text);
        NormalizeHelper::removeDoubles($text);
        NormalizeHelper::removeSpacesFromBeginningAndEnd($text);

        return $text;
    }

    /**
     * Checks if a line for example: only contains special characters
     * @param string $line
     * @return bool
     */
    private static function checkBrokenLine(string $line) : bool
    {
        $regexes = [
            '/^([^abcdefghijklmnopqrstuvwxyz]+)/mi',
            '/([^.?!]+)$/m',
            '/ ([abcdefghijklmnopqrstuvwxyz]{1,2}).$/mi',
            '/([^abcdefghijklmnopqrstuvwxyz.]+)[.!?]$/mi',
            '/[()]/m',
            '/((.+),(.+)){3,}/mi'
        ];
        foreach($regexes as $regex)
        {
            if (RegexHelper::check($regex, $line))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a line contains a bad word (which is not twitter suitable)
     * @param string $line
     * @return bool
     */
    private static function containsBadWords(string $line) : bool
    {
        $words = [
            'fuck',
            'nigger',
            '22ch',
            '4ch',
            'dick',
            'm00t',
            'twoot',
            'twot',
            'jew',
            'discord',
            'gay',
            'fag',
            'Ylyl',
            'mfw',
            'tfw',
            'tomoko',
            'trip',
            'dub',
            'chan',
            'downvote',
            'upvote',
            'cringe',
            'reddit',
            'meme',
            'porn'
        ];
        $line = strtolower($line);
        return str_replace($words, '', $line) !== $line;
    }

    /**
     * Removes bad lines from a string
     * @param string $text
     * @return string
     */
    public static function removeDirt(string $text, bool $checkBad = true) : string
    {
        $lines = explode("\n", $text);
        $text = "";
        foreach ($lines as $line)
        {
            $line = trim($line);

            if (self::checkBrokenLine($line) || ($checkBad && self::containsBadWords($line)))
            {
                continue;
            }
            if ($text !== "")
            {
                $text .= "\n";
            }

            $text .= $line;
        }
        return $text;
    }

    /**
     * Does a quick cleanup of a given string
     * @param string $text
     * @return string
     */
    public static function quickCleanup(string $text) : string
    {
        NormalizeHelper::removeLinks($text);
        NormalizeHelper::removeReferences($text);
        NormalizeHelper::removeInvalidCharacters($text);
        NormalizeHelper::removeDoubles($text);
        NormalizeHelper::removeSpacesFromBeginningAndEnd($text);

        return $text;
    }

    /**
     * Cleanes up the text some more.
     * @param string $text
     * @return string
     */
    public static function advancedCleanup(string &$text) : string
    {
        $text = preg_replace('/["§$%{}\[\]]/m', '', $text);

        $text = str_replace(".", ".\n", $text);
        $text = str_replace("?", "?\n", $text);
        $text = str_replace("!", "!\n", $text);
        $text = str_replace(">", "\n>", $text);
        $text = str_replace(";", ";\n", $text);

        $text = preg_replace('/^( +)/m', '', $text);
        $text = preg_replace('/( +)$/m', '', $text);
        $text = preg_replace('/(\n+)/m', "\n", $text);

        $text = preg_replace('/([a-z\d])$/mi', '$0.', $text);

        preg_match_all('/^.+ .+ .+$/mi', $text, $matches);
        $text = implode("\n", $matches[0]);

        $lines = explode("\n", $text);
        foreach($lines as &$line) {
            $line = ucfirst($line);
        }
        $text = implode("\n", $lines);

        return $text;
    }

    /**
     * Removes references from a given text (>234).
     * @param string $text
     * @return string
     */
    public static function removeReferences(string &$text) : string
    {
        $regex = '/>>(\d*)/m';
        $text = RegexHelper::remove($regex, $text);

        return $text;
    }

    /**
     * Removes invalid characters from a string
     * @param string $text
     * @return string
     */
    public static function removeInvalidCharacters(string &$text) : string
    {
        $regex = '/(?![a-zA-Z0-9!,\s?>.])/m';
        $text = RegexHelper::remove($regex, $text);

        return $text;
    }

    /**
     * Removes spaces from the beginning and the end of each line.
     * @param string $text
     * @return string
     */
    public static function removeSpacesFromBeginningAndEnd(string &$text) : string
    {
        $regex = '/^( +)|( +)$/m';
        $text = preg_replace($regex, ' ', $text);

        return $text;
    }

    /**
     * Removes double Whitespaces and double Newlines from a given text.
     * @param string $text
     * @return string
     */
    public static function removeDoubles(string &$text) : string
    {
        $text = preg_replace('/[\r\n]{2,}/', "\n", $text);
        $text = preg_replace('/ {2,}/m', ' ', $text);

        return $text;
    }

    /**
     * Resolves the important information from a text and resolves HTML.
     * @param string $text
     * @return string
     */
    public static function resolveHTML(string &$text) : string
    {
        // Replacing <br/> with newlines
        $text = str_replace("<br/>", "\n", $text);

        // <br> seperates the body from things like "Your fortune is: ...", so we only want the first part which is actually made by a human
        $text = explode("<br>", $text)[0];

        // Removing all html tags
        $text = strip_tags($text);

        // Resolving some html escapes
        $text = str_replace("&gt;", ">", $text);
        $text = str_replace("&lt;", "<", $text);

        // Removing spoiler tags
        $text = str_replace("[spoiler]", "", $text);
        $text = str_replace("[/spoiler]", "", $text);

        return $text;
    }

    /**
     * Removes empty elements from a string array.
     * @param array $array
     * @return array
     */
    public static function removeEmptyElements(array &$array) : array
    {
        $array = array_filter($array, function($value) { return $value !== ''; });
        return $array;
    }

    /**
     * Lowering words that are written in capslock (HELLO -> hello).
     * @param string $text
     * @return string
     */
    public static function lowercaseTheUpercase(string &$text) : string
    {
        $regex = '/([A-Z]{2,})/m';
        preg_match_all($regex, $text, $matches);
        foreach($matches as $match)
        {
            $text = str_replace($match[0], strtolower($match[0]), $text);
        }

        return $text;
    }

    /**
     * Removes links from a string
     * @param string $text
     * @return string
     */
    public static function removeLinks(string &$text) : string
    {
        $text = RegexHelper::remove('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', $text);

        return $text;
    }
}