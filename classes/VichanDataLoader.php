<?php

class VichanDataLoader
{
    public static function rebuildData()
    {
        global $config;

        // Including vichans config (for database credentials)
        require_once(dirname(__FILE__) . "/../../../www/inc/instance-config.php");

        // Making sure the cache file is deleted
        unlink(SOURCE_FILE);

        $text = "";

        // Establishing database connection
        $connt = mysqli_connect($config['db']['server'], $config['db']['user'], $config['db']['password'], $config['db']['database']);

        // Getting a list of all boards there are and saving their name inside an array ($boards)
        $sql = "SELECT uri FROM ch_boards";

        $boards = [];

        $result = $connt->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $boards[] = $row["uri"];
            }
        }

        // New array for all the posts there are (each posts text will be saved in that array)
        $texts = [];

        // Go through each board
        foreach($boards as $board)
        {
            // Selecting the Version without markup in it
            $sql = "SELECT body_nomarkup FROM ch_posts_".$board." WHERE password='' AND files IS NULL";

            $result = $connt->query($sql);

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $body = $row["body_nomarkup"];

                    // Normalizing the post.
                    $body = NormalizeHelper::initialNormalize($body);

                    // Adding cleaned up body to the text array
                    $texts[] = $body;
                }
            }
        }

        // Closing the database connection (we dont need it anymore at this point)
        $connt->close();

        // Getting all json files (for archived threads)
        $jsonFiles = Helper::listJsonFiles("../archive");

        // Iterating over all the json files
        foreach($jsonFiles as $file)
        {
            // Decoding the json file
            $json = json_decode(file_get_contents($file));

            // If the json doesn't look like it should, we will skip that file
            if (!isset($json->posts))
            {
                continue;
            }

            // Iterating over all the posts of that thread
            foreach($json->posts as $post)
            {
                // Getting the body
                $content = $post->com;

                // Normalizing the post.
                $content = NormalizeHelper::initialNormalize($content);

                // Adding cleaned up body to the text array
                $texts[] = $content;
            }
        }

        // Removing empty texts
        NormalizeHelper::removeEmptyElements($texts);

        // Imploding the array and separating the posts by two new lines
        $text = implode("\n\n", $texts);

        // Quickly making sure the post it clean.
        $text = NormalizeHelper::quickCleanup($text);

        // Finally saving that text to a file (so we dont have to regenerate it every time)
        file_put_contents(SOURCE_FILE, $text);

        // Redirecting to the same site without the "g" get parameter so we dont cache on every reload
        header("Location: generator.php");
        exit;
    }
}