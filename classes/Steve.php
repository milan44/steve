<?php

class Steve
{
    /**
     * @var string
     */
    public static $text;

    public static function generateSentences() : string
    {
        // Getting cached data from data.txt
        $origtext = file_get_contents(SOURCE_FILE);

        // Removing unicode to avoid utf-8 errors
        self::$text = preg_replace('/[\x10-\x1F\x80-\xFF]/', '', $origtext);

        // Building markov table and then generating text with a length of 200 characters

        $length = 200;

        $markov = new Markov();
        $markov->generateTable(self::$text, 4);
        $generated = $markov->generateText($length);

        /*
         * Cleaning up generated String with some more regex functions.
         */
        $generated = NormalizeHelper::quickCleanup($generated);
        NormalizeHelper::advancedCleanUp($generated);

        /*
         * Removing possibly useless lines.
         */

        $generated = NormalizeHelper::removeDirt($generated);

        NormalizeHelper::removeUsedLines($generated);

        /*
         * Lets get at least 5 lines of generated text
         */
        while(sizeof(explode("\n", $generated)) < 5)
        {
            $generated2 = $markov->generateText($length);

            /*
             * Cleaning up generated String with some more regex functions.
             */
            $generated2 = NormalizeHelper::quickCleanup($generated2);
            NormalizeHelper::advancedCleanUp($generated2);

            /*
             * Removing possibly useless lines.
             */
            $generated2 = NormalizeHelper::removeDirt($generated2);

            /*
             * Adding a newline if the text isn't empty
             */
            if (!empty(trim($generated)))
            {
                $generated .= "\n";
            }
            $generated = trim($generated . $generated2);

            NormalizeHelper::removeUsedLines($generated);
        }

        return $generated;
    }
}