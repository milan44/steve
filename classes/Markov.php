<?php
define(
    'LINE_TERMINATORS',
    [
        '.',
        '?',
        '!',
        ';'
    ]
);
define(
    'SENTENCE_SPLITTERS',
    [
        ',',
        ':'
    ]
);
define(
    'WORD_COMBINERS',
    [
        '&',
        '/'
    ]
);

class Markov
{
    /**
     * @var array
     */
    private $table;

    private function normalizeText(string $text) : string
    {
        $text = str_replace('’', '\'', $text);
        $text = str_replace(['“', '”'], '"', $text);

        return $text;
    }

    private function quoteCharacter(string $char) : string
    {
        return preg_quote($char, '/');
    }

    private function tokenize(string $text) : array
    {
        $text = $this->normalizeText($text);
        $markov = $this;

        $quoteChar = function(string $char) use ($markov) : string {
            return $markov->quoteCharacter($char);
        };

        $specialCharacters = implode('', array_map($quoteChar, LINE_TERMINATORS));
        $specialCharacters .= implode('', array_map($quoteChar, SENTENCE_SPLITTERS));
        $specialCharacters .= implode('', array_map($quoteChar, WORD_COMBINERS));

        $wordRegex = '/((?i)tar\.gz(?-i))|([a-zA-Z0-9]\.){2,}|[A-Z\-\']{2,}(?![a-z])|[A-Z\-\'][a-z\-\']+(?=[A-Z][A-Za-z])|[\'\w\-]+|[' .
                     $specialCharacters . ']+/m';
        preg_match_all($wordRegex, $text, $matches, PREG_SET_ORDER, 0);

        return array_map(
            function($m) {
                return $m[0];
            },
            $matches
        );
    }

    private function combineStrings(string $str, string $add) : string
    {
        if (in_array($add, LINE_TERMINATORS)) {
            return $str . $add . "\n";
        } else {
            if (in_array($add, SENTENCE_SPLITTERS)) {
                return $str . $add . " ";
            } else {
                if (in_array($add, WORD_COMBINERS)) {
                    return $str . " " . $add . " ";
                }
            }
        }

        return $str . " " . $add;
    }

    public function generateTable(string $text, int $look_forward = 4)
    {
        $words = $this->tokenize($text);
        $table = [];
        $lookedAtWords = [];

        for ($i = 0; $i < sizeof($words) - $look_forward; $i++) {
            $word = $words[$i];

            for ($j = 1; $j < $look_forward; $j++) {
                $word = $this->combineStrings($word, $words[$i + $j]);
            }

            $lookedAtWords[] = $word;
        }

        for ($i = 0; $i < sizeof($lookedAtWords) - 1; $i++) {
            $currentWord = $lookedAtWords[$i];
            $nextWord = $lookedAtWords[$i + 1];

            if (!isset($table[$currentWord])) {
                $table[$currentWord] = [];
            }
            if (!isset($table[$currentWord][$nextWord])) {
                $table[$currentWord][$nextWord] = 0;
            }

            $table[$currentWord][$nextWord]++;
        }

        $this->table = $table;
    }

    public function generateText(int $length) : string
    {
        $word = array_rand($this->table);
        while ($word !== ucfirst($word)) {
            $word = array_rand($this->table);
        }

        $result = $word;

        while (strlen($result) < $length) {
            $nextWord = $this->returnWeightedWord($this->table[$word]);

            if ($nextWord) {
                $result .= ' ' . $nextWord;
                $word = $nextWord;
            } else {
                $word = array_rand($this->table);
                while ($word === ucfirst($word)) {
                    $word = array_rand($this->table);
                }
            }
        }

        return $result;
    }

    private function returnWeightedWord($array)
    {
        if (!$array) {
            return false;
        }

        shuffle($array);

        $total = array_sum($array);
        $rand = mt_rand(1, $total);

        foreach ($array as $item => $weight) {
            if ($rand <= $weight) {
                return $item;
            }
            $rand -= $weight;
        }

        return null;
    }
}